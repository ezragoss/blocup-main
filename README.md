# BlocUp

Cryptocurrency mining as a method of supporting local mutual aid efforts. [Forked](https://simple.wikipedia.org/wiki/Fork_(software_development)) from the New Inquiry's [BailBloc](https://github.com/thenewinquiry/bailbloc) project, which pioneered the concept to assist with fuindraising for with the Brooklyn Bail Fund.

#### Front Matter
1. [What is *mining*?](#section-1)

### What is *mining*?

*Section under construction*

### How is this different from [BailBloc](https://github.com/thenewinquiry/bailbloc)

*Section under construction*

### How can I use this to raise money for my project?

*Section under construction*

#### How much can I expect to raise?

*Section under construction*

### How can I download *BlocUp*

*Section under construction*